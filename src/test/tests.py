import unittest

from pyspark.sql import SparkSession
from pyspark.sql.functions import collect_list
from pyspark.sql.types import StructField, StructType, DoubleType, StringType

from src.main.python.transform import cities_tmpr_data, biggest_cities_data


class SparkETLTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # initializing spark session for test-cases
        cls.spark = (SparkSession
                     .builder
                     .master("local[1]")
                     .appName("pyspark-tests")
                     .getOrCreate())

        # preparing schema for dataframe which needed in test-cases
        input_schema = StructType([
            StructField('city', StringType()),
            StructField('wthr_date', StringType()),
            StructField('id', StringType()),
            StructField('avg_tmpr_c', DoubleType()),
        ])

        # preparing data for dataframe which needed in test-cases
        input_data = [
            ('Moscow', '2021-06-10', '1', 10.0),
            ('Moscow', '2021-06-10', '2', 30.0),
            ('Moscow', '2021-06-10', '2', 20.0),
            ('Moscow', '2021-06-10', '3', 30.0),
            ('Moscow', '2021-06-11', '1', 10.0),
            ('Moscow', '2021-06-11', '1', 20.0),
            ('Kazan', '2021-06-10', '2', 20.0),
            ('Kazan', '2021-06-11', '2', 25.0),
        ]

        # creating dataframe which needed in test-cases
        cls.input_df = cls.spark.createDataFrame(data=input_data, schema=input_schema)

    def testCityTmprData(self):

        """ Checking aggregated data after grouping dataframe by city and date """

        df = cities_tmpr_data(weather_df=self.input_df, spark=self.spark)

        self.assertEqual(df.count(), 4)

        self.assertEqual(df.select(collect_list('hotels_count')).first()[0], [1, 1, 3, 1])
        self.assertEqual(df.select(collect_list('avg_tmpr')).first()[0], [20.0, 25.0, 22.5, 15.0])
        self.assertEqual(df.select(collect_list('max_tmpr')).first()[0], [20.0, 25.0, 30.0, 20.0])
        self.assertEqual(df.select(collect_list('min_tmpr')).first()[0], [20.0, 25.0, 10.0, 10.0])

    def testBiggestCitiesData(self):

        """ Checking cities with biggest count of hotels """

        df = biggest_cities_data(weather_df=self.input_df, spark=self.spark)

        self.assertEqual(df.count(), 2)
        self.assertEqual(df.select(collect_list('hotels_count')).first()[0], [3, 1])

    @classmethod
    def tearDownClass(cls):
        # stopping spark session
        cls.spark.stop()