from pyspark.sql import DataFrame, SparkSession


def cities_tmpr_data(weather_df: DataFrame, spark: SparkSession) -> DataFrame:
    weather_df.createOrReplaceTempView("hotel_weather")

    query = """
    SELECT city, wthr_date, COUNT(DISTINCT id) as hotels_count,
    AVG(avg_tmpr_c) as avg_tmpr, MAX(avg_tmpr_c) as max_tmpr, MIN(avg_tmpr_c) as min_tmpr
    FROM hotel_weather
    GROUP BY city, wthr_date
    """

    city_tmpr_df = spark.sql(query)

    return city_tmpr_df


def biggest_cities_data(weather_df: DataFrame, spark: SparkSession, city_limit: int = 10) -> DataFrame:
    weather_df.createOrReplaceTempView("hotel_weather")
    query = f"""
    SELECT city, COUNT(DISTINCT id) as hotels_count
    FROM hotel_weather
    GROUP BY city
    ORDER BY COUNT(DISTINCT id) DESC
    LIMIT {city_limit}
    """

    biggest_cities_df = spark.sql(query)

    return biggest_cities_df


def biggest_cities_tmpr_data(
    weather_df: DataFrame,
    biggest_cities_df: DataFrame,
    spark: SparkSession
) -> DataFrame:

    weather_df.createOrReplaceTempView("hotel_weather")
    biggest_cities_df.createOrReplaceTempView("biggest_cities")

    query = """
    SELECT city, wthr_date, COUNT(DISTINCT id) as hotels_count, 
    AVG(avg_tmpr_c) as avg_tmpr, MAX(avg_tmpr_c) as max_tmpr, MIN(avg_tmpr_c) as min_tmpr
    FROM hotel_weather
    WHERE city IN (SELECT city FROM biggest_cities)
    GROUP BY city, wthr_date
    ORDER BY city, wthr_date
    """

    biggest_cities_tmpr_df = spark.sql(query)

    return biggest_cities_tmpr_df


