import os
from collections import defaultdict

import matplotlib.pyplot as plt
from pyspark import SparkConf
from pyspark.sql import SparkSession, DataFrame
from transform import cities_tmpr_data, biggest_cities_data, biggest_cities_tmpr_data


# setup spark config for access to azure storage
def setup_spark_config() -> SparkConf:
    conf = SparkConf()
    conf.set("fs.azure.account.auth.type.bd201stacc.dfs.core.windows.net", os.environ.get("AUTH_TYPE"))
    conf.set("fs.azure.account.oauth.provider.type.bd201stacc.dfs.core.windows.net", os.environ.get("PROVIDER_TYPE"))
    conf.set("fs.azure.account.oauth2.client.id.bd201stacc.dfs.core.windows.net", os.environ.get("CLIENT_ID"))
    conf.set("fs.azure.account.oauth2.client.secret.bd201stacc.dfs.core.windows.net", os.environ.get("CLIENT_SECRET"))
    conf.set("fs.azure.account.oauth2.client.endpoint.bd201stacc.dfs.core.windows.net", os.environ.get("CLIENT_ENDPOINT"))
    return conf


# visualize aggregated city-date-hotels-weather data
def visualize_hotel_weather_data(biggest_cities_tmpr_df: DataFrame) -> None:
    df_iterator = biggest_cities_tmpr_df.rdd.toLocalIterator()
    plot_data = defaultdict(list)

    # collecting data for each of the biggest cities
    for row in df_iterator:
        plot_data[row["city"]].append(
            (row["wthr_date"], row["hotels_count"], row["avg_tmpr"], row["max_tmpr"], row["min_tmpr"])
        )

    chart_path = os.environ.get("CHART_PATH")

    # building chart for each of the biggest cities
    for city, data in plot_data.items():
        # setting size and resolution for chart
        plt.figure(figsize=(10, 8), dpi=200)
        # setting x-label values
        days = [row[0] for row in data]
        # setting y-label values (for multiple[4] lines)
        plt.plot(days, [row[1] for row in data], label="Hotels count")
        plt.plot(days, [row[2] for row in data], label="Avg tmpr")
        plt.plot(days, [row[3] for row in data], label="Max tmpr")
        plt.plot(days, [row[4] for row in data], label="Min tmpr")
        # rotate x-date-labels for correct display
        plt.xticks(rotation=60)
        # setting title for chart of the city
        plt.title(city)
        plt.legend()
        # saving the chart of the city
        plt.savefig(f"{chart_path}/{city}_chart.png")
        # cleaning plot
        plt.close()


if __name__ == "__main__":
    # setup config for spark session
    conf = setup_spark_config()

    # spark session's initialization
    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    # reading dataframe with hotels and weather
    weather_hotel_df = spark.read.parquet(os.environ.get("WEATHER_HOTEL_PATH"))
    # caching the weather-hotel-df
    weather_hotel_df.persist()

    # forming aggregated city-temperature data
    # showing the data
    # showing execution plan of the data
    cities_tmpr_df = cities_tmpr_data(weather_hotel_df, spark)
    cities_tmpr_df.show()
    cities_tmpr_df.explain(extended=True)

    # forming biggest cities (cities with biggest count of hotels)
    # caching the data
    # showing the data
    # showing execution plan of the data
    biggest_cities_df = biggest_cities_data(weather_hotel_df, spark)
    biggest_cities_df.persist()
    biggest_cities_df.show()
    biggest_cities_df.explain(extended=True)

    # forming aggregated city-temperature data for biggest cities
    # caching the data
    # showing the data
    # showing execution plan of the data
    biggest_cities_tmpr_df = biggest_cities_tmpr_data(weather_hotel_df, biggest_cities_df, spark)
    biggest_cities_tmpr_df.persist()
    biggest_cities_tmpr_df.show()
    biggest_cities_tmpr_df.explain(extended=True)

    # building temperature-chart for each city
    visualize_hotel_weather_data(biggest_cities_tmpr_df)







