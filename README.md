**Azure account is forbidden now for Russia (validating debit card is impossible, tried many times with many cards),**

therefore **ADLS gen2** and **Databricks** with **AutoLoader** (for streaming) is not available for me.

I started job locally (and tested step-by-step with interactive `pyspark-shell`, unit tests are also provided).

And also I visualized result data with **matplotlib** instead of Databricks visualizations.



**Instruction for local use**

Install virtual env in `venv/` directory

Fill in `venv/bin/activate` you own env-variables' values from `env.example`

Activate virtual env and your env-variables by `source venv/bin/activate`

Install requirements via `pip install -r requirements.txt`

To make the script-file (with **spark-submit** command) executable:

`sudo chmod +x ./scripts/local_submit_run.sh`

To run job locally:

`./scripts/local_submit_run.sh`

To run interactive pyspark-shell:

`sudo chmod +x ./scripts/local_pyspark_shell.sh`

`./scripts/local_pyspark_shell.sh`

**Screenshots with comments**

Calculated count of distinct hotels, avg/max/min temperature for each city each day:

![](screenshots/cities_tmpr_data.png)

Calculating 10 cities with biggest count of distinct hotels:

![](screenshots/biggest_cities_data.png)

Calculated count of distinct hotels, avg/max/min temperature for each city each day (only for the 10 biggest cities), ordered by city and date:

![](screenshots/biggest_cities_tmpr_data.png)

Builded charts with count of distinct hotels, avg/max/min temperature for each of the biggest cities:

![](screenshots/Amsterdam_chart.png)

![](screenshots/Barcelona_chart.png)

![](screenshots/Houston_chart.png)

![](screenshots/London_chart.png)

![](screenshots/Milan_chart.png)

![](screenshots/New_York_chart.png)

![](screenshots/Paddington_chart.png)

![](screenshots/Paris_chart.png)

![](screenshots/San_Diego_chart.png)

![](screenshots/Springfield_chart.png)