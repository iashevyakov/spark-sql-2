== Parsed Logical Plan ==

# Taking only first 10 cities
'GlobalLimit 10
+- 'LocalLimit 10

# Sorting by count of distinct hotels (desc)

   +- 'Sort ['COUNT(distinct 'id) DESC NULLS LAST], true
# Grouping by city
      +- 'Aggregate ['city], ['city]
         +- 'UnresolvedRelation [hotel_weather], [], false

== Analyzed Logical Plan ==
city: string
GlobalLimit 10
+- LocalLimit 10
   +- Project [city#3]
      +- Sort [count(distinct id#6)#107L DESC NULLS LAST], true

# Grouping by city, selecting "city" and hotels_count
         +- Aggregate [city#3], [city#3, count(distinct id#6) AS count(distinct id#6)#107L]
            +- SubqueryAlias hotel_weather
               +- View (`hotel_weather`, [address#0,avg_tmpr_c#1,avg_tmpr_f#2,city#3,country#4,geoHash#5,id#6,latitude#7,longitude#8,name#9,wthr_date#10,year#11,month#12,day#13])
                  +- Relation [address#0,avg_tmpr_c#1,avg_tmpr_f#2,city#3,country#4,geoHash#5,id#6,latitude#7,longitude#8,name#9,wthr_date#10,year#11,month#12,day#13] parquet

== Optimized Logical Plan ==
GlobalLimit 10
+- LocalLimit 10
   +- Project [city#3]

# Sorting by count of distinct hotels (desc)

      +- Sort [count(distinct id#6)#107L DESC NULLS LAST], true
# Grouping by city, selecting "city" and hotels_count

         +- Aggregate [city#3], [city#3, count(distinct id#6) AS count(distinct id#6)#107L]
            +- Project [city#3, id#6]
               +- Relation [address#0,avg_tmpr_c#1,avg_tmpr_f#2,city#3,country#4,geoHash#5,id#6,latitude#7,longitude#8,name#9,wthr_date#10,year#11,month#12,day#13] parquet

== Physical Plan ==
AdaptiveSparkPlan isFinalPlan=false
+- TakeOrderedAndProject(limit=10, orderBy=[count(distinct id#6)#107L DESC NULLS LAST], output=[city#3])

# Hash realization of aggregating

   +- HashAggregate(keys=[city#3], functions=[count(distinct id#6)], output=[city#3, count(distinct id#6)#107L])
      +- Exchange hashpartitioning(city#3, 200), ENSURE_REQUIREMENTS, [id=#1141]
         +- HashAggregate(keys=[city#3], functions=[partial_count(distinct id#6)], output=[city#3, count#289L])
            +- HashAggregate(keys=[city#3, id#6], functions=[], output=[city#3, id#6])
               +- Exchange hashpartitioning(city#3, id#6, 200), ENSURE_REQUIREMENTS, [id=#1137]
                  +- HashAggregate(keys=[city#3, id#6], functions=[], output=[city#3, id#6])
                     +- Project [city#3, id#6]

# Parquet file scanning
                        +- FileScan parquet [city#3,id#6,year#11,month#12,day#13] Batched: true, DataFilters: [], Format: Parquet, Location: InMemoryFileIndex(1 paths)[abfss://m13sparkstreaming@bd201stacc.dfs.core.windows.net/hotel-weather], PartitionFilters: [], PushedFilters: [], ReadSchema: struct<city:string,id:string>
