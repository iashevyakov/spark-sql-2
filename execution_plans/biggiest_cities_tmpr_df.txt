== Parsed Logical Plan ==
'Sort ['city ASC NULLS FIRST, 'wthr_date ASC NULLS FIRST], true

# Aggregating by "city" and "date", selecting "city", "date", "hotels_count", "avg/max/min temperature"

+- 'Aggregate ['city, 'wthr_date], ['city, 'wthr_date, 'COUNT(distinct 'id) AS hotels_count#113, 'AVG('avg_tmpr_c) AS avg_tmpr#114, 'MAX('avg_tmpr_c) AS max_tmpr#115, 'MIN('avg_tmpr_c) AS min_tmpr#116]

# Considering cities only from biggest-cities-list

   +- 'Filter 'city IN (list#117 [])
      :  +- 'Project ['city]
      :     +- 'UnresolvedRelation [biggest_cities], [], false
      +- 'UnresolvedRelation [hotel_weather], [], false

== Analyzed Logical Plan ==
city: string, wthr_date: string, hotels_count: bigint, avg_tmpr: double, max_tmpr: double, min_tmpr: double
Sort [city#3 ASC NULLS FIRST, wthr_date#10 ASC NULLS FIRST], true
+- Aggregate [city#3, wthr_date#10], [city#3, wthr_date#10, count(distinct id#6) AS hotels_count#113L, avg(avg_tmpr_c#1) AS avg_tmpr#114, max(avg_tmpr_c#1) AS max_tmpr#115, min(avg_tmpr_c#1) AS min_tmpr#116]
   +- Filter city#3 IN (list#117 [])
      :  +- Project [city#121]
      :     +- SubqueryAlias biggest_cities
      :        +- View (`biggest_cities`, [city#121])
      :           +- GlobalLimit 10
      :              +- LocalLimit 10
      :                 +- Project [city#121]
      :                    +- Sort [count(distinct id#6)#107L DESC NULLS LAST], true
      :                       +- Aggregate [city#121], [city#121, count(distinct id#124) AS count(distinct id#6)#107L]
      :                          +- SubqueryAlias hotel_weather
      :                             +- View (`hotel_weather`, [address#118,avg_tmpr_c#119,avg_tmpr_f#120,city#121,country#122,geoHash#123,id#124,latitude#125,longitude#126,name#127,wthr_date#128,year#129,month#130,day#131])
      :                                +- Relation [address#118,avg_tmpr_c#119,avg_tmpr_f#120,city#121,country#122,geoHash#123,id#124,latitude#125,longitude#126,name#127,wthr_date#128,year#129,month#130,day#131] parquet
      +- SubqueryAlias hotel_weather
         +- View (`hotel_weather`, [address#0,avg_tmpr_c#1,avg_tmpr_f#2,city#3,country#4,geoHash#5,id#6,latitude#7,longitude#8,name#9,wthr_date#10,year#11,month#12,day#13])
            +- Relation [address#0,avg_tmpr_c#1,avg_tmpr_f#2,city#3,country#4,geoHash#5,id#6,latitude#7,longitude#8,name#9,wthr_date#10,year#11,month#12,day#13] parquet

== Optimized Logical Plan ==
Sort [city#3 ASC NULLS FIRST, wthr_date#10 ASC NULLS FIRST], true
+- Aggregate [city#3, wthr_date#10], [city#3, wthr_date#10, count(distinct id#6) AS hotels_count#113L, avg(avg_tmpr_c#1) AS avg_tmpr#114, max(avg_tmpr_c#1) AS max_tmpr#115, min(avg_tmpr_c#1) AS min_tmpr#116]
   +- Join LeftSemi, (city#3 = city#121)
      :- Project [avg_tmpr_c#1, city#3, id#6, wthr_date#10]
      :  +- Relation [address#0,avg_tmpr_c#1,avg_tmpr_f#2,city#3,country#4,geoHash#5,id#6,latitude#7,longitude#8,name#9,wthr_date#10,year#11,month#12,day#13] parquet
      +- GlobalLimit 10
         +- LocalLimit 10
            +- Project [city#121]

# Sorting by hotels count (desc)

               +- Sort [count(distinct id#6)#107L DESC NULLS LAST], true

# Aggregating by city, selecting city and hotels count

                  +- Aggregate [city#121], [city#121, count(distinct id#124) AS count(distinct id#6)#107L]
                     +- Project [city#121, id#124]
                        +- Relation [address#118,avg_tmpr_c#119,avg_tmpr_f#120,city#121,country#122,geoHash#123,id#124,latitude#125,longitude#126,name#127,wthr_date#128,year#129,month#130,day#131] parquet

== Physical Plan ==
AdaptiveSparkPlan isFinalPlan=true
+- == Final Plan ==
   *(7) Sort [city#3 ASC NULLS FIRST, wthr_date#10 ASC NULLS FIRST], true, 0
   +- AQEShuffleRead coalesced
      +- ShuffleQueryStage 5
         +- Exchange rangepartitioning(city#3 ASC NULLS FIRST, wthr_date#10 ASC NULLS FIRST, 200), ENSURE_REQUIREMENTS, [id=#1096]
            +- *(6) HashAggregate(keys=[city#3, wthr_date#10], functions=[avg(avg_tmpr_c#1), max(avg_tmpr_c#1), min(avg_tmpr_c#1), count(distinct id#6)], output=[city#3, wthr_date#10, hotels_count#113L, avg_tmpr#114, max_tmpr#115, min_tmpr#116])
               +- AQEShuffleRead coalesced
                  +- ShuffleQueryStage 4
                     +- Exchange hashpartitioning(city#3, wthr_date#10, 200), ENSURE_REQUIREMENTS, [id=#1066]
                        +- *(5) HashAggregate(keys=[city#3, wthr_date#10], functions=[merge_avg(avg_tmpr_c#1), merge_max(avg_tmpr_c#1), merge_min(avg_tmpr_c#1), partial_count(distinct id#6)], output=[city#3, wthr_date#10, sum#162, count#163L, max#165, min#167, count#170L])
                           +- *(5) HashAggregate(keys=[city#3, wthr_date#10, id#6], functions=[merge_avg(avg_tmpr_c#1), merge_max(avg_tmpr_c#1), merge_min(avg_tmpr_c#1)], output=[city#3, wthr_date#10, id#6, sum#162, count#163L, max#165, min#167])
                              +- AQEShuffleRead coalesced
                                 +- ShuffleQueryStage 3
                                    +- Exchange hashpartitioning(city#3, wthr_date#10, id#6, 200), ENSURE_REQUIREMENTS, [id=#1022]
                                       +- *(4) HashAggregate(keys=[city#3, wthr_date#10, id#6], functions=[partial_avg(avg_tmpr_c#1), partial_max(avg_tmpr_c#1), partial_min(avg_tmpr_c#1)], output=[city#3, wthr_date#10, id#6, sum#162, count#163L, max#165, min#167])
                                          +- *(4) BroadcastHashJoin [city#3], [city#121], LeftSemi, BuildRight, false
                                             :- *(4) Project [avg_tmpr_c#1, city#3, id#6, wthr_date#10]
                                             :  +- *(4) ColumnarToRow
                                             :     +- FileScan parquet [avg_tmpr_c#1,city#3,id#6,wthr_date#10,year#11,month#12,day#13] Batched: true, DataFilters: [], Format: Parquet, Location: InMemoryFileIndex(1 paths)[abfss://m13sparkstreaming@bd201stacc.dfs.core.windows.net/hotel-weather], PartitionFilters: [], PushedFilters: [], ReadSchema: struct<avg_tmpr_c:double,city:string,id:string,wthr_date:string>
                                             +- BroadcastQueryStage 2
                                                +- BroadcastExchange HashedRelationBroadcastMode(List(input[0, string, true]),false), [id=#943]
                                                   +- TakeOrderedAndProject(limit=10, orderBy=[count(distinct id#6)#107L DESC NULLS LAST], output=[city#121])
                                                      +- *(3) HashAggregate(keys=[city#121], functions=[count(distinct id#124)], output=[city#121, count(distinct id#6)#107L])
                                                         +- AQEShuffleRead coalesced
                                                            +- ShuffleQueryStage 1
                                                               +- Exchange hashpartitioning(city#121, 200), ENSURE_REQUIREMENTS, [id=#852]
                                                                  +- *(2) HashAggregate(keys=[city#121], functions=[partial_count(distinct id#124)], output=[city#121, count#174L])
                                                                     +- *(2) HashAggregate(keys=[city#121, id#124], functions=[], output=[city#121, id#124])
                                                                        +- AQEShuffleRead coalesced
                                                                           +- ShuffleQueryStage 0
                                                                              +- Exchange hashpartitioning(city#121, id#124, 200), ENSURE_REQUIREMENTS, [id=#752]
                                                                                 +- *(1) HashAggregate(keys=[city#121, id#124], functions=[], output=[city#121, id#124])
                                                                                    +- *(1) Project [city#121, id#124]
                                                                                       +- *(1) ColumnarToRow
                                                                                          +- FileScan parquet [city#121,id#124,year#129,month#130,day#131] Batched: true, DataFilters: [], Format: Parquet, Location: InMemoryFileIndex(1 paths)[abfss://m13sparkstreaming@bd201stacc.dfs.core.windows.net/hotel-weather], PartitionFilters: [], PushedFilters: [], ReadSchema: struct<city:string,id:string>
+- == Initial Plan ==
   Sort [city#3 ASC NULLS FIRST, wthr_date#10 ASC NULLS FIRST], true, 0
   +- Exchange rangepartitioning(city#3 ASC NULLS FIRST, wthr_date#10 ASC NULLS FIRST, 200), ENSURE_REQUIREMENTS, [id=#704]
      +- HashAggregate(keys=[city#3, wthr_date#10], functions=[avg(avg_tmpr_c#1), max(avg_tmpr_c#1), min(avg_tmpr_c#1), count(distinct id#6)], output=[city#3, wthr_date#10, hotels_count#113L, avg_tmpr#114, max_tmpr#115, min_tmpr#116])
         +- Exchange hashpartitioning(city#3, wthr_date#10, 200), ENSURE_REQUIREMENTS, [id=#701]

# Hash realization of aggregating

            +- HashAggregate(keys=[city#3, wthr_date#10], functions=[merge_avg(avg_tmpr_c#1), merge_max(avg_tmpr_c#1), merge_min(avg_tmpr_c#1), partial_count(distinct id#6)], output=[city#3, wthr_date#10, sum#162, count#163L, max#165, min#167, count#170L])
               +- HashAggregate(keys=[city#3, wthr_date#10, id#6], functions=[merge_avg(avg_tmpr_c#1), merge_max(avg_tmpr_c#1), merge_min(avg_tmpr_c#1)], output=[city#3, wthr_date#10, id#6, sum#162, count#163L, max#165, min#167])
                  +- Exchange hashpartitioning(city#3, wthr_date#10, id#6, 200), ENSURE_REQUIREMENTS, [id=#697]
                     +- HashAggregate(keys=[city#3, wthr_date#10, id#6], functions=[partial_avg(avg_tmpr_c#1), partial_max(avg_tmpr_c#1), partial_min(avg_tmpr_c#1)], output=[city#3, wthr_date#10, id#6, sum#162, count#163L, max#165, min#167])
                        +- BroadcastHashJoin [city#3], [city#121], LeftSemi, BuildRight, false
                           :- Project [avg_tmpr_c#1, city#3, id#6, wthr_date#10]
                           :  +- FileScan parquet [avg_tmpr_c#1,city#3,id#6,wthr_date#10,year#11,month#12,day#13] Batched: true, DataFilters: [], Format: Parquet, Location: InMemoryFileIndex(1 paths)[abfss://m13sparkstreaming@bd201stacc.dfs.core.windows.net/hotel-weather], PartitionFilters: [], PushedFilters: [], ReadSchema: struct<avg_tmpr_c:double,city:string,id:string,wthr_date:string>
                           +- BroadcastExchange HashedRelationBroadcastMode(List(input[0, string, true]),false), [id=#693]
                              +- TakeOrderedAndProject(limit=10, orderBy=[count(distinct id#6)#107L DESC NULLS LAST], output=[city#121])
                                 +- HashAggregate(keys=[city#121], functions=[count(distinct id#124)], output=[city#121, count(distinct id#6)#107L])
                                    +- Exchange hashpartitioning(city#121, 200), ENSURE_REQUIREMENTS, [id=#689]
                                       +- HashAggregate(keys=[city#121], functions=[partial_count(distinct id#124)], output=[city#121, count#174L])
                                          +- HashAggregate(keys=[city#121, id#124], functions=[], output=[city#121, id#124])
                                             +- Exchange hashpartitioning(city#121, id#124, 200), ENSURE_REQUIREMENTS, [id=#685]
                                                +- HashAggregate(keys=[city#121, id#124], functions=[], output=[city#121, id#124])
                                                   +- Project [city#121, id#124]
                                                      +- FileScan parquet [city#121,id#124,year#129,month#130,day#131] Batched: true, DataFilters: [], Format: Parquet, Location: InMemoryFileIndex(1 paths)[abfss://m13sparkstreaming@bd201stacc.dfs.core.windows.net/hotel-weather], PartitionFilters: [], PushedFilters: [], ReadSchema: struct<city:string,id:string>
